import 'package:flutter/material.dart';
import 'package:flutter_componentes/src/pages/facebook_page.dart';
import 'package:flutter_componentes/src/pages/home_page.dart';
import 'package:flutter_componentes/src/pages/instagram_page.dart';
// import 'package:flutter_componentes/src/pages/home_temp.dart';
 
void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Componentes App',
      debugShowCheckedModeBanner: false,
      //  home: HomePage(),
      initialRoute: '/',
      routes: <String, WidgetBuilder>{
        '/' : ( BuildContext context ) => HomePage(),
        'facebook' : ( BuildContext context ) => FacebookPage(),
        'instagram' : ( BuildContext context ) => InstagramPage(),
      },
    );
  }
}