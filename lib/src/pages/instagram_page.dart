import 'package:flutter/material.dart';

class InstagramPage extends StatelessWidget {
  TextEditingController nameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.pink,
        appBar: AppBar(
          title: Text('Instagram'),
        ),
        body: Padding(
            padding: EdgeInsets.all(10),
            child: ListView(
              children: <Widget>[
                Container(
                    child: new Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [new Image.asset("assets/instagram.png")])),
                Container(
                  padding: EdgeInsets.all(10),
                  child: TextField(
                    controller: nameController,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'User name',
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                  child: TextField(
                    obscureText: true,
                    controller: passwordController,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Password',
                    ),
                  ),
                ),
                Container(
                    height: 50,
                    padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                    margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                    child: RaisedButton(
                      textColor: Colors.white,
                      color: Colors.transparent,
                      child: Text('Log in'),
                      onPressed: () {
                        print(nameController.text);
                        print(passwordController.text);
                      },
                    )),
                    FlatButton(
                  onPressed: () {
                    //forgot password screen
                  },
                  textColor: Colors.white,
                  child: Text('Forgot your login detail? Get help signing in.'),
                ),
              ],
            )));
  }
}